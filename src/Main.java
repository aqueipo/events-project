import java.util.Arrays;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import com.aqueipo.events.services.*;


public class Main {

    public static boolean arrayContainElement(String[] arr, String targetValue) {
        return Arrays.asList(arr).contains(targetValue);
    }

    public static void main(String args[]) {
        // Comandos permitidos
        String[] availableCommands = new String[] { "OFF",  "ON" };

        // Generamos los servicios
        EventService service = new EventService();
        EventConsole console = new EventConsole();
        console.registerSuscriber(service);

        // Leemos de stdin
        BufferedReader br = null;
        System.out.println("Introduzca comandos a continuacion (q y ENTER para salir):");

        try {
            br = new BufferedReader(new InputStreamReader(System.in));

            while (true) {

                String input = br.readLine();

                if ("q".equals(input)) {
                    System.out.println("Exit!");
                    System.exit(0);
                }

                String[] arr = input.split("[^a-zA-Z0-9]+");

                if (arr.length > 0 && (arr.length != 2 || !arrayContainElement(availableCommands, arr[0]))) {
                    // Error en la sintaxis del comando
                    System.out.println("ERROR Uso de comando: <ON|OFF> <Nombre_dispositivo>");
                } else {
                    // Ejecutamos comando
                    String command = arr[0];
                    String device = arr[1];
                    console.setParameters(command, device);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
