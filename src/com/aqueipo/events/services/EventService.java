package com.aqueipo.events.services;

import java.util.Date;
import java.util.HashMap;
import com.aqueipo.events.interfaces.EventSuscriber;
import com.aqueipo.events.models.EventDevice;

/**
 * Created by aqueipo on 2/12/16.
 */
public class EventService implements EventSuscriber {
    private HashMap<String, EventDevice> eventsMap;

    // Constructor
    public EventService() {
        this.eventsMap = new HashMap<String, EventDevice>();
    }

    // Métodos de la interfaz EventSuscriber
    @Override
    public void newEvent(String command, String device) {
        //System.out.println("EventService: new command: "
        //        + command);
        EventDevice event = eventsMap.get(device);
        if (event != null && command.equals("OFF")) {
            event.setEndTime(new Date());
            long timeElapsed = (event.getEndTime().getTime() - event.getInitialTime().getTime()) / 1000;
            eventsMap.remove(device);
            System.out.println("ALARM " + event.getDevice() + " " + timeElapsed);
        } else if (command.equals("ON")) {
            event = new EventDevice(device);
            eventsMap.put(device, event);
        }
    }

    // getters-setters
    public HashMap<String, EventDevice> getEventsMap() {
        return eventsMap;
    }

    public void setEventsMap(HashMap<String, EventDevice> eventsMap) {
        this.eventsMap = eventsMap;
    }
}
