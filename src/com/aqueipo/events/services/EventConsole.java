package com.aqueipo.events.services;

import com.aqueipo.events.interfaces.EventPublisher;
import com.aqueipo.events.interfaces.EventSuscriber;

import java.util.ArrayList;

/**
 * Created by aqueipo on 2/12/16.
 */
public class EventConsole implements EventPublisher {
    private ArrayList<EventSuscriber> suscribers = new ArrayList<EventSuscriber>();
    private String command;
    private String device;

    // Métodos de la interfaz EventPublisher
    @Override
    public void registerSuscriber(EventSuscriber suscriber) {
        suscribers.add(suscriber);

    }

    @Override
    public void removeSuscriber(EventSuscriber suscriber) {
        suscribers.remove(suscriber);

    }

    @Override
    public void notifySuscribers() {
        for (EventSuscriber suscriber : suscribers) {
            //System.out
            //        .println("Notifying Suscribers new event");
            suscriber.newEvent(this.command, this.device);
        }
    }

    // getters-setters
    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
        this.notifySuscribers();
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public void setParameters(String command, String device) {
        this.device = device;
        this.command = command;
        this.notifySuscribers();
    }

    public ArrayList<EventSuscriber> getSuscribers() {
        return suscribers;
    }

    public void setSuscribers(ArrayList<EventSuscriber> suscribers) {
        this.suscribers = suscribers;
    }
}
