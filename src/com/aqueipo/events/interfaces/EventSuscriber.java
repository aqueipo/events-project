package com.aqueipo.events.interfaces;

/**
 * Created by aqueipo on 2/12/16.
 */
public interface EventSuscriber {

    /**
     * Método que recibe un suscriptor y ha de implementar
     * @param command El comando a ejecutar
     * @param device El dispositivo sobre el que se ejecuta
     */
    public void newEvent(String command, String device);
}
