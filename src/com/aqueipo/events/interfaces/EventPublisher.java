package com.aqueipo.events.interfaces;

/**
 * Created by aqueipo on 2/12/16.
 */

public interface EventPublisher {
    /**
     * Metodo para el registro de un suscriptor
     * @param suscriber
     */
    public void registerSuscriber(EventSuscriber suscriber);

    /**
     * Método para el eliminar el registro de un suscriptor
     * @param suscriber
     */
    public void removeSuscriber(EventSuscriber suscriber);

    /**
     * Método para notificar a los suscriptores
     */
    public void notifySuscribers();
}
