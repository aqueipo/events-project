package com.aqueipo.events.models;

import java.util.Date;

/**
 * Created by aqueipo on 2/12/16.
 */
public class EventDevice {
    private String device;
    private Date initialTime;
    private Date endTime;

    // Constructor
    public EventDevice(String device) {
        this.device = device;
        this.initialTime = new Date();
        this.endTime = null;
    }

    // getters-setters
    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public Date getInitialTime() {
        return initialTime;
    }

    public void setInitialTime(Date initialTime) {
        this.initialTime = initialTime;
    }
}
